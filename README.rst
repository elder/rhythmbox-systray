Overview
========

This plugin allows you to control Rhythmbox from the system tray. It requires
Rhythmbox 2.90+.

.. image:: http://eldermarco.fedorapeople.org/files/systray/menu-icon.png
   :align: center
   :alt: Menu


Features
========

With this plugin you can

* Mute/Unmute playback;
* Stop playback after current track;
* Minimize Rhythmbox to the system tray;
* See the current playing song when you hover the tray icon;
* Lower or raise the volume by scrolling up or down over the tray icon;
* Control basic Rhythmbox features (play/pause, stop and go to the
  previous/next track).


Dependencies
============

In order to install and use this plugin, you need to install the following
packages:

* gettext;
* intltool;
* Rhythmbox 2.90+ (compiled with python support);


Installation
============

Download the latest version in the download area:

`<https://bitbucket.org/elder/rhythmbox-systray/downloads/>`__

Extract its contents and run the following command in a terminal window:


::

    $ ./configure --prefix=/usr --libdir=<libdir>
    $ make
    # make install


Where *<libdir>* is */usr/lib* or */usr/lib64*, depending on where the
rhythmbox's plugins directory is installed (*/usr/lib/rhythmbox/plugins* or
*/usr/lib64/rhythmbox/plugins*).


Activate the plugin
===================

Open or restart Rhythmbox and go to *Edit > Plugins > System Tray Icon* and
activate the plugin.


See also
========

* `Tray Icon <http://mendhak.github.com/rhythmbox-tray-icon/>`_ - another Rhythmbox tray icon plugin.
