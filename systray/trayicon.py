# -*- coding: utf-8 -*-
# vim: expandtab:tabstop=4:shiftwidth=4
#
# This file is part of rhythmbox-systray.
#
# Copyright (C) 2012-2013 - Elder Marco <eldermarco@gmail.com>
#
# rhythmbox-systray is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# rhythmbox-systray is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rhythmbox-systray. If not, see <http://www.gnu.org/licenses/>.
import os
import sys

from gi.repository import RB
from gi.repository import Gtk, Gdk, GObject

from . import i18n
from .menu import SysTrayMenu


class SysTrayIcon (object):

    def __init__ (self, shell, plugin):
        self.shell = shell
        self.plugin = plugin
        self.sp = self.shell.props.shell_player
        self.window = self.shell.get_property ('window')
        self.window_id = None

        self.systrayicon = Gtk.StatusIcon ()
        self.systrayicon.set_title ('Rhythmbox')
        self.systrayicon.set_from_icon_name ('rhythmbox')
        self.systrayicon.set_tooltip_text (self.window.get_title ())
        self.systrayicon.set_visible (True)

        self.systraymenu = SysTrayMenu (self.shell, self.plugin)
        self.connect_signals ()

    def connect_signals (self):
        '''
        Connect all signals associated with the status icon and the
        Rhythmbox's main window
        '''
        self.systrayicon_ids = (
            self.systrayicon.connect ('scroll-event',
                                       self.scroll_cb),
            self.systrayicon.connect ('button-press-event',
                                       self.button_press_cb),
            self.systrayicon.connect ('popup-menu',
                                       self.systraymenu.display_cb)
        )
        self.window_id = self.sp.connect ('window-title-changed',
                                           self.window_title_changed_cb)

        return True

    def disconnect_signals (self):
        '''
        Disconnect all signals associated with the status icon and the
        Rhythmbox's main window
        '''
        for id in self.systrayicon_ids:
            self.systrayicon.disconnect (id)
        self.sp.disconnect (self.window_id)
        self.window_id = None

    def scroll_cb (self, widget, event):
        '''
        Lower or raises Rhythmbox's volume by 5%
        '''
        volume = self.sp.get_volume ()[1]
        volume += 0.05 if event.direction == Gdk.ScrollDirection.UP else -0.05
        if volume > 1 or volume < 0:
            volume = 1.0 if volume > 1 else 0.0

        self.sp.set_volume (volume)

    def button_press_cb (self, widget, event):
        '''
        Hide/Show the Rhythmbox's main window when the user left-click
        on the status icon.
        '''
        visible = self.window.get_visible ()
        if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 1:
            if visible:
                self.window.hide ()
            else:
                self.window.show ()

            return True

    def window_title_changed_cb (self, player, playing):
        '''
        Set the tooltip text
        '''
        self.systrayicon.set_tooltip_text (self.window.get_title ())

    def deactivate (self):
        '''
        Deactivate status icon and the popup menu
        '''
        self.disconnect_signals ()
        self.systraymenu.disconnect_signals ()
        self.systrayicon.set_visible (False)
        self.sp = None
        self.shell = None
        self.window = None
        self.systrayicon = None
        self.systraymenu = None
