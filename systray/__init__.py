# -*- coding: utf-8 -*-
# vim: expandtab:tabstop=4:shiftwidth=4
#
# This file is part of rhythmbox-systray.
#
# Copyright (C) 2012-2013 - Elder Marco <eldermarco@gmail.com>
#
# rhythmbox-systray is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# rhythmbox-systray is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rhythmbox-systray. If not, see <http://www.gnu.org/licenses/>.
import os
import sys

from gi.repository import RB
from gi.repository import GObject, Peas

from . import i18n
from .trayicon import SysTrayIcon


class SysTrayPlugin (GObject.Object, Peas.Activatable):
    __gtype_name__ = 'SysTrayPlugin'
    object = GObject.property (type=GObject.Object)

    def __init__ (self):
        GObject.Object.__init__ (self)

    def do_activate (self):
        self.shell = self.object

        # We need to handle the delete-event signal in order to make possible
        # to us to minimize Rhythmbox to the system tray when the user tries
        # to close the main window.
        self.window = self.shell.get_property ('window')
        self.window_id = self.window.connect ('delete-event',
                                               self.delete_event_cb)

        self.systrayicon = SysTrayIcon (self.shell, self)

    def do_deactivate (self):
        self.shell = None
        self.systrayicon.deactivate ()
        self.window.disconnect (self.window_id)
        del self.window
        del self.systrayicon

    def delete_event_cb (self, widget, event):
        '''
        Hide the main window
        '''
        self.window.hide ()
        return True
