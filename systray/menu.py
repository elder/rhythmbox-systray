# -*- coding: utf-8 -*-
# vim: expandtab:tabstop=4:shiftwidth=4
#
# This file is part of rhythmbox-systray.
#
# Copyright (C) 2012-2013 - Elder Marco <eldermarco@gmail.com>
#
# rhythmbox-systray is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# rhythmbox-systray is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rhythmbox-systray. If not, see <http://www.gnu.org/licenses/>.
import os
import sys

import rb
from gi.repository import RB
from gi.repository import Gtk, GObject

from . import i18n


class SysTrayMenu (object):

    def __init__ (self, shell, plugin):
        self.shell = shell
        self.plugin = plugin
        self.sp = shell.props.shell_player
        self.player = self.sp.props.player
        self.volume = None
        self.pc_id = None
        self.psc_id = None


        if not self.sp.get_playing ()[1]:
            playpause_label = _('Play')
            playpause_stock = Gtk.STOCK_MEDIA_PLAY
        else:
            playpause_label = _('Pause')
            playpause_stock = Gtk.STOCK_MEDIA_PAUSE


        self.menu = Gtk.ActionGroup ('SysTrayMenuActions')
        self.menu.add_actions ([
            ('Previous', Gtk.STOCK_MEDIA_PREVIOUS, _('Previous track'),
                  None, None, self.previous_cb),
            ('PlayPause', playpause_stock, playpause_label,
                  None, None, self.playpause_cb),
            ('Stop', Gtk.STOCK_MEDIA_STOP, _('Stop'),
                  None, None, self.stop_cb),
            ('Next', Gtk.STOCK_MEDIA_NEXT, _('Next track'),
                  None, None, self.next_cb),
            ('Quit', Gtk.STOCK_QUIT, _('Quit'),
                  None, None, self.quit_cb)
        ])

        action = Gtk.ToggleAction ('MuteUnmute', _('Mute'), None, None)
        action.set_visible (True)
        action.set_active (self.sp.get_mute ()[1])
        action.connect ('toggled', self.muteunmute_cb)
        self.menu.add_action (action)

        action = Gtk.ToggleAction ('StopAfter',
                                   _('Stop after this track'),
                                   None, None)
        action.set_visible (True)
        action.set_active (False)
        action.connect ('toggled', self.stop_after_cb)
        action.set_sensitive (self.sp.get_playing ()[1])
        self.menu.add_action (action)

        self.ui = Gtk.UIManager ()
        self.ui.add_ui_from_file (rb.find_plugin_file (self.plugin,
                                                       'systray-menu.ui'))
        self.ui.insert_action_group (self.menu)

        self.connect_signals ()

    def connect_signals (self):
        self.pc_id = self.sp.connect ('playing-changed',
                                       self.playing_changed_cb)

    def disconnect_signals (self):
        self.sp.disconnect (self.pc_id)
        self.pc_id = None

    def previous_cb (self, widget):
        '''
        If the current song has been playing for more than 3 seconds,
        restarts it. Otherwise, goes back to the previous song.
        '''
        self.sp.do_previous ()
        return True

    def playpause_cb (self, widget):
        '''
        Toggles between playing and paused state. Set the label and the
        stock id, depending on the state.
        '''
        success = self.sp.playpause (True)
        if success:
            action = self.menu.get_action ('PlayPause')
            if not self.sp.get_playing ()[1]:
                action.set_label (_('Play'))
                action.set_stock_id (Gtk.STOCK_MEDIA_PLAY)
            else:
                action.set_label (_('Pause'))
                action.set_stock_id (Gtk.STOCK_MEDIA_PAUSE)

        return True

    def stop (self):
        '''
        Stops the music player
        '''
        if self.sp.get_playing ()[1]:
            self.sp.stop ()
        return True

    def stop_cb (self, widget):
        '''
        Stops playback (callback function)
        '''
        self.stop ()
        return True

    def next_cb (self, widget):
        '''
        Skips to the next track
        '''
        self.sp.do_next ()
        return True

    def muteunmute_cb (self, widget):
        '''
        Mute/Unmute the music player
        '''
        playing = self.sp.get_playing ()[1]
        active = self.menu.get_action ('MuteUnmute').get_active ()

        if active:
            self.sp.set_mute (True)
            self.volume = self.sp.get_volume ()[1]
        else:
            self.sp.set_mute (False)
            if playing:
                self.sp.set_volume (self.volume)

        return True

    def stop_after_cb (self, widget):
        '''
        Allows user to stop the music player after current track
        '''
        active = self.menu.get_action ('StopAfter').get_active ()
        if active:
            self.psc_id = self.sp.connect ('playing-song-changed',
                                            lambda player, entry: self.stop ())
        else:
            if self.psc_id is not None:
                self.sp.disconnect (self.psc_id)
                self.psc_id = None

        return True

    def quit_cb (self, widget):
        '''
        Quit the music player
        '''
        self.shell.quit ()

    def playing_changed_cb (self, player, playing):
        '''
        Set the label and the icon of the PlayPause action. Also, set
        active and sensitive properties for the StopAfter action.
        '''
        playpause = self.menu.get_action ('PlayPause')
        stopafter = self.menu.get_action ('StopAfter')

        if playing:
            playpause.set_label (_('Pause'))
            playpause.set_stock_id (Gtk.STOCK_MEDIA_PAUSE)
            stopafter.set_sensitive (True)

        else:
            playpause.set_label (_('Play'))
            playpause.set_stock_id (Gtk.STOCK_MEDIA_PLAY)
            stopafter.set_active (False)
            stopafter.set_sensitive (False)

    def display_cb (self, icon, button, time):
        '''
        Display the system tray popup menu.
        '''
        menu = self.ui.get_widget ('/PopupMenu')
        menu.popup (None, None,
                    Gtk.StatusIcon.position_menu, icon,
                    button, time)
        return True

    def deactivate (self):
        '''
        Deactivate the menu
        '''
        self.sp = None
        self.shell = None
        self.plugin = None
        self.player = None
        self.disconnect_signals ()
